CREATE TABLE rooms
(
    id       bigserial primary key,
    owner_id bigint      not null,
    name     varchar(50) not null,

    constraint fk_owner foreign key (owner_id) references users (id) on delete cascade
);


CREATE TABLE users_rooms
(
    user_id bigint not null,
    room_id bigint not null,

    constraint fk_user foreign key (user_id) references users (id) on delete cascade,
    constraint fk_room foreign key (room_id) references rooms (id) on delete cascade,
    constraint pk_user_room primary key (user_id, room_id)
);

CREATE TABLE announcements
(
    id            bigserial primary key,
    creation_date timestamp   not null default now(),
    text          varchar(500) not null,
    room_id       bigint      not null,

    constraint fk_room foreign key (room_id) references rooms (id) on delete cascade
);

CREATE TABLE desk
(
    id      bigserial primary key,
    name    varchar(50) not null,
    room_id bigint      not null,

    constraint fk_desk foreign key (room_id) references rooms (id) on delete cascade
);

CREATE TABLE desk_objects
(
    id      bigserial primary key,
    data    jsonb  not null,
    desk_id bigint not null,
    constraint fk_desk foreign key (desk_id) references desk (id) on delete cascade
);
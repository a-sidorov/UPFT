CREATE TABLE resource
(
    id  bigserial primary key,
    link_to_resource  varchar(50) not null,
    describe varchar(50) not null
);



CREATE TABLE tasks
(
    id          bigserial primary key,
    name        varchar(50) not null,
    describe    varchar(100) not null,
    comment     varchar(50) not null,
    room_id     bigint      not null,
    resource_id bigint      null,
    deadline    timestamp   not null,

    constraint fk_room foreign key (room_id) references rooms (id) on delete cascade,
    constraint fk_res foreign key (resource_id) references resource (id) on delete cascade
);
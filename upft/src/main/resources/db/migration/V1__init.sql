CREATE TABLE users
(
    id                bigserial primary key,
    first_name        varchar(50)   not null,
    second_name       varchar(50)   not null,
    login             varchar(50)   not null unique check ( length(login) > 8 ),
    password          varchar(1000) not null,
    registration_date timestamp     not null
);

CREATE TABLE roles
(
    id   bigserial primary key,
    type varchar(15) not null unique check (type = 'ROLE_STUDENT' or type = 'ROLE_TEACHER' or type = 'ROLE_ADMIN')
);

CREATE TABLE Users_Roles
(
    user_id bigint not null,
    role_id bigint not null,

    constraint fk_user foreign key (user_id) references users (id) on delete cascade,
    constraint fk_role foreign key (role_id) references roles (id) on delete cascade,
    constraint pk_user_role primary key (user_id, role_id)
);

insert into roles(id, type)
values (1, 'ROLE_STUDENT');
insert into roles(id, type)
values (2, 'ROLE_TEACHER');
insert into roles(id, type)
values (3, 'ROLE_ADMIN');
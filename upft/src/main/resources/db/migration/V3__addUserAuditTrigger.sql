CREATE TABLE users_audit
(
    id          bigserial primary key,
    operation   char(6)       not null,
    stamp       timestamp     not null,
    first_name  varchar(50)   not null,
    second_name varchar(50)   not null,
    login       varchar(50)   not null check ( length(login) > 8 ),
    password    varchar(1000) not null
);

CREATE OR REPLACE FUNCTION process_users_audit() RETURNS TRIGGER AS
$users_audit$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        INSERT INTO users_audit
        SELECT nextval('users_audit_id_seq'), 'DELETE', now(), OLD.first_name, OLD.second_name, OLD.login, OLD.password;
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO users_audit
        SELECT nextval('users_audit_id_seq'), 'UPDATE', now(), NEW.first_name, NEW.second_name, NEW.login, NEW.password;
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO users_audit
        SELECT nextval('users_audit_id_seq'), 'INSERT', now(), NEW.first_name, NEW.second_name, NEW.login, NEW.password;
        RETURN NEW;
    END IF;
    RETURN NULL; -- возвращаемое значение для триггера AFTER игнорируется
END;
$users_audit$ LANGUAGE plpgsql;

CREATE TRIGGER user_audit
    AFTER INSERT OR UPDATE OR DELETE
    ON users
    FOR EACH ROW
EXECUTE PROCEDURE process_users_audit();
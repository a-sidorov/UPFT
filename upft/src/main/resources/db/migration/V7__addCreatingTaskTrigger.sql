CREATE OR REPLACE FUNCTION process_task_creation() RETURNS TRIGGER AS
$task_creation$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO announcements
        SELECT nextval('announcements_id_seq'),
               now(),
               'New task ' || NEW.name || ' deadline: ' || to_char(NEW.deadline, 'yyyy-mm-dd hh24:mi:ss'),
               NEW.room_id;
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$task_creation$ LANGUAGE plpgsql;

CREATE TRIGGER task_creation
    AFTER INSERT
    ON tasks
    FOR EACH ROW
EXECUTE PROCEDURE process_task_creation();
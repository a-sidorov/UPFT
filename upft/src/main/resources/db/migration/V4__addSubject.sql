CREATE TABLE subjects
(
    id       bigserial primary key,
    type     varchar(15) not null unique,
    describe varchar(100) not null
);

insert into subjects
values (nextval('subjects_id_seq'), 'none', 'unknown subject');

ALTER TABLE rooms
    add subject_id bigint not null default 1;

alter table rooms
    add constraint fk_subjects foreign key (subject_id) references subjects (id) on delete cascade;
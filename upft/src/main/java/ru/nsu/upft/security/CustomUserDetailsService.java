package ru.nsu.upft.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.nsu.upft.entity.user.User;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.services.UserService;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userEntity = null;
        try {
            userEntity = userService.getUserByLogin(username);
        } catch (DataNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
        return CustomUserDetails.fromUserEntityToCustomUserDetails(userEntity);
    }
}

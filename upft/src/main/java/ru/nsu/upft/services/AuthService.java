package ru.nsu.upft.services;

import com.nimbusds.oauth2.sdk.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nsu.upft.dto.UserDtoRequest;
import ru.nsu.upft.dto.UserDtoResponse;
import ru.nsu.upft.dto.mapper.UserMapper;
import ru.nsu.upft.entity.user.Role;
import ru.nsu.upft.entity.user.User;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.exception.UserExistException;
import ru.nsu.upft.repository.RoleRepository;
import ru.nsu.upft.repository.UserRepository;
import ru.nsu.upft.security.jwt.JwtProvider;

import javax.security.auth.message.AuthException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;

    private final PasswordEncoder passwordEncoder;

    private final JwtProvider jwtProvider;

    public UserDtoResponse createUser(UserDtoRequest userDto) throws DataNotFoundException, UserExistException {
        // TODO: 13.03.2021 uncle boи читстая арихитектура, interactor
        // TODO: 13.03.2021 Eric Evans DDD, Red book

        Set<Role> roles = new HashSet<>();

        for (String roleStr : userDto.getRoles()) {
            Role role = roleRepository.findByType(roleStr)
                    .orElseThrow(() -> new DataNotFoundException(roleStr + " ROLE_NOT_EXIST"));
            roles.add(role);
        }


        var user = userMapper.dtoToUser(userDto);

        if (userRepository.findByLogin(user.getLogin()).isPresent()) {
            throw new UserExistException(user.getLogin() + " LOGIN_EXIST");
        }
        ;

        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.getRoles().addAll(roles);

        return userMapper.userToDtoResponse(
                userRepository.save(user)
        );
    }

    public String createAuthToken(String authorization) throws AuthException {
        authorization = cutAuthCode(authorization);
        Map<String, String> loginAndPasswordMap = getLoginAndPassword(authorization);
        String login = loginAndPasswordMap.get("login");
        String password = loginAndPasswordMap.get("password");

        var userEntity = userRepository.findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("USER_EXIST"));


        if (!checkPasswordUser(password, userEntity)) throw new AuthException("Неверный логин или пароль");

        return jwtProvider.generateToken(userEntity.getLogin());
    }

    private String cutAuthCode(String authorization) throws AuthException {
        if (StringUtils.isNotBlank(authorization) && authorization.startsWith("Basic")) {
            authorization = authorization.substring(6);
            if (StringUtils.isBlank(authorization)) {
                throw new AuthException("Пустые данные авторизации");
            }
        } else {
            throw new AuthException("Пустые данные авторизации");
        }
        return authorization;
    }

    private Map<String, String> getLoginAndPassword(String authHeader) throws BadCredentialsException {
        String customerCredentials = (convertFromBase64(authHeader));
        String login = customerCredentials.split(":")[0];
        String password = customerCredentials.split(":")[1];
        Map<String, String> loginAndPasswordMap = new HashMap<>();
        loginAndPasswordMap.put("login", login);
        loginAndPasswordMap.put("password", password);
        return loginAndPasswordMap;
    }

    private boolean checkPasswordUser(String password, User userEntity) {
        return passwordEncoder.matches(password, userEntity.getPassword());
    }

    private String convertFromBase64(String base64String) {
        byte[] bytes = base64String.getBytes(StandardCharsets.UTF_8);
        bytes = Base64.getDecoder().decode(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }
}

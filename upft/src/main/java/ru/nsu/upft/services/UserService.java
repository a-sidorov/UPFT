package ru.nsu.upft.services;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.upft.dto.UserDtoRequest;
import ru.nsu.upft.dto.UserDtoResponse;
import ru.nsu.upft.dto.UserHistoryDto;
import ru.nsu.upft.dto.mapper.UserMapper;
import ru.nsu.upft.entity.user.Role;
import ru.nsu.upft.entity.user.User;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.repository.RoleRepository;
import ru.nsu.upft.repository.UserAuditRepository;
import ru.nsu.upft.repository.UserRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    @Getter
    private final RoleRepository roleRepository;

    @Getter
    private final UserMapper userMapper;

    private final UserAuditRepository userAuditRepository;

    @Transactional
    public void remove(String login) throws DataNotFoundException {
        var removableUser = userRepository.findByLogin(login)
                .orElseThrow(() -> new DataNotFoundException("USER_NOT_EXIST"));
        userRepository.delete(removableUser);
    }

    public UserDtoResponse getUser(Long userId) throws DataNotFoundException {
        return userMapper.userToDtoResponse(userRepository.findById(userId).orElseThrow(() -> new DataNotFoundException("USER_EXIST")));
    }

    public List<UserDtoResponse> getUsersByRole(String role) {
        var users = userRepository.findAllByRole(role);
        List<UserDtoResponse> responseList = new ArrayList<>();

        users.forEach(user -> responseList.add(userMapper.userToDtoResponse(user)));

        return responseList;
    }

    @Transactional
    public UserDtoResponse updateUser(UserDtoRequest userDtoRequest, String login) throws DataNotFoundException {
        var updatableUser = userRepository.findByLogin(login)
                .orElseThrow(() -> new DataNotFoundException("USER_NOT_EXIST"));

        if (userDtoRequest.getFirstName() != null) {
            updatableUser.setFirstName(userDtoRequest.getFirstName());
        }

        if (userDtoRequest.getSecondName() != null) {
            updatableUser.setSecondName(userDtoRequest.getSecondName());
        }

        if (userDtoRequest.getRoles() != null) {

            Set<Role> roles = new HashSet<>();

            for (String roleStr : userDtoRequest.getRoles()) {
                Role role = roleRepository.findByType(roleStr)
                        .orElseThrow(() -> new DataNotFoundException(roleStr + " ROLE_NOT_EXIST"));

                roles.add(role);
            }

            updatableUser.setRoles(roles);

        }

        return userMapper.userToDtoResponse(userRepository.save(updatableUser));
    }

    public List<UserDtoResponse> getAll() {
        var users = userRepository.findAll();
        List<UserDtoResponse> responseList = new ArrayList<>();

        users.forEach(user -> responseList.add(userMapper.userToDtoResponse(user)));

        return responseList;
    }

    public User getUserByLogin(String login) throws DataNotFoundException {
        return userRepository.findByLogin(login).orElseThrow(() -> new DataNotFoundException("USER_NOT_EXIST"));
    }

    public List<UserHistoryDto> getHistory(String date) {
        var data = userAuditRepository.getAllUserHistory(Timestamp.valueOf(date));
        List<UserHistoryDto> dtos = new ArrayList<>();
        data.forEach(userHistory -> dtos.add(UserHistoryDto.toDto(userHistory)));
        return dtos;
    }
}

package ru.nsu.upft.services;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.upft.entity.desk.DeskObject;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.repository.DeskObjectRepository;
import ru.nsu.upft.repository.DeskRepository;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeskService {
    private final DeskRepository deskRepository;
    private final DeskObjectRepository deskObjectRepository;

    public String addObject(@NonNull Long deskId, String data) {
        var desk = deskRepository.findById(deskId).orElse(null);
        if (desk == null) {
            return null;
        }
        var obj = new DeskObject(data, desk);
        return deskObjectRepository.save(obj).getData();
    }

    public List<String> getAllObjects(@NonNull Long deskId) throws DataNotFoundException {
        var desk = deskRepository.findById(deskId).orElseThrow(() -> new DataNotFoundException("DESK_NOT_EXISTS"));
        var objs = deskObjectRepository.findDeskObjectByDesk(desk);
        List<String> stringList = new ArrayList<>();
        objs.forEach(obj -> stringList.add(obj.getData()));
        return stringList;
    }

}

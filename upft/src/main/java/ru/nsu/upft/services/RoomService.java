package ru.nsu.upft.services;

import com.sun.istack.Nullable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.upft.dto.*;
import ru.nsu.upft.dto.mapper.RoomDtoMapper;
import ru.nsu.upft.entity.desk.Desk;
import ru.nsu.upft.entity.room.*;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.repository.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RoomService {
    private final RoomRepository repository;
    private final UserService userService;
    private final RoomDtoMapper dtoMapper;
    private final SubjectRepository subjectRepository;
    private final DeskRepository deskRepository;
    private final TaskRepository taskRepository;
    private final ResourceRepository resourceRepository;
    private final AnnouncementRepository announcementRepository;

    public Long createRoom(String userName, @NonNull RoomDto roomDto) throws DataNotFoundException {
        var user = userService.getUserByLogin(userName);
        var room = new Room();

        room.setOwner(user);

        room.setName(roomDto.getName());

        room.setDescribe(roomDto.getDescription());
        var type = roomDto.getSubject().getType();

        var existingSubject = subjectRepository.findByType(type).orElse(null);

        if (existingSubject == null) {
            existingSubject = new Subject();
            existingSubject.setType(type);
            existingSubject.setDescribe(roomDto.getSubject().getDescribe());
            existingSubject = subjectRepository.save(existingSubject);
        }

        room.setSubject(existingSubject);
        room = repository.save(room);
        var desk = new Desk("default");
        desk.setRoom(room);
        desk = deskRepository.save(desk);
        room.setRoomDesk(desk);

        return repository.save(room).getId();
    }

    public Long createTask(String userName, TaskDto taskDto, Long roomId) throws DataNotFoundException {
        var task = new Task();
        task.setComment(taskDto.getComment());
        task.setName(taskDto.getName());
        ;
        task.setDeadline(taskDto.getDeadline());
        task.setDescribe(taskDto.getDescription());
        var room = repository.findByIdAndOwnerLogin(roomId, userName)
                .orElseThrow(() -> new DataNotFoundException("ROOM_NOT_EXIST"));
        task.setRoom(room);
        var resDto = taskDto.getResource();
        Resource res = null;
        if (resDto != null) {
            res = new Resource(resDto.getLinkToResourse(), resDto.getDescribe());
            res = resourceRepository.save(res);
        }
        task.setResource(res);
        return taskRepository.save(task).getId();
    }

    public Long addAnnouncement(String userName, Long roomId, AnnouncementRequestDto dto) throws DataNotFoundException {
        var room = repository.findByIdAndOwnerLogin(roomId, userName)
                .orElseThrow(() -> new DataNotFoundException("ROOM_NOT_EXIST"));
        var announcement = new Announcements();
        announcement.setText(dto.getText());
        announcement.setRoom(room);
        announcementRepository.save(announcement);
        return announcement.getId();
    }

    public List<AllRoomDto> getRooms(String login) {
        var rooms = repository.findRoomsByOwnerLogin(login);
        List<AllRoomDto> allRoomDtos = new ArrayList<>();
        rooms.forEach(room -> allRoomDtos.add(dtoMapper.toDto(room)));
        return allRoomDtos;
    }

    public List<AllRoomDto> getStudentRoom(String login) {
        var rooms = repository.findAll();
        List<AllRoomDto> allRoomDtos = new ArrayList<>();
        rooms.forEach(room -> {
            if (room.hasMember(login)) {
                allRoomDtos.add(dtoMapper.toDto(room));
            }
        });
        return allRoomDtos;
    }

    public List<AnnouncementResponceDto> getAllRoomAnnouncements(Long roomId) throws DataNotFoundException {
        var room = repository.findById(roomId)
                .orElseThrow(() -> new DataNotFoundException("ROOM_NOT_EXIST"));


        var list = announcementRepository.findAllByRoom(room);
        List<AnnouncementResponceDto> newList = new ArrayList<>();
        for (Announcements a : list) {
            newList.add(new AnnouncementResponceDto(a.getText(), a.getCreationDate()));
        }
        return newList;

    }

    public Room getRoom(String login, Long id) throws DataNotFoundException {
        var room = repository.findByIdAndOwnerLogin(id, login).orElse(null);
        if (room != null) {
            return room;
        }

        return repository.findByIdAndMemberLogin(id, login).orElseThrow(() -> new DataNotFoundException("ROOM_NOT_EXIST"));
    }

    public void inviteStudents(String login, Long roomId, String userLogin) throws DataNotFoundException {
        var room = repository.findByIdAndOwnerLogin(roomId, login)
                .orElseThrow(() -> new DataNotFoundException("ROOM_NOT_EXIST"));

        room.getUsers().add(userService.getUserByLogin(userLogin));

        repository.save(room);
    }

    public void removeRoom(String login, Long roomId) throws DataNotFoundException {
        var room = repository.findByIdAndOwnerLogin(roomId, login)
                .orElseThrow(() -> new DataNotFoundException("ROOM_NOT_EXIST"));
        repository.delete(room);
    }

    public List<SubjectDto> getAllSubjects() {
        var list = subjectRepository.findAll();
        List<SubjectDto> allSubjectDtos = new ArrayList<>();
        list.forEach(subject -> allSubjectDtos.add(new SubjectDto(subject.getType(), subject.getDescribe())));
        return allSubjectDtos;
    }

    public String createSubject(SubjectDto dto) {
        var type = dto.getType();

        var existingSubject = subjectRepository.findByType(type).orElse(null);

        if (existingSubject == null) {
            existingSubject = new Subject();
            existingSubject.setType(type);
            existingSubject.setDescribe(dto.getDescribe());
            existingSubject = subjectRepository.save(existingSubject);
            return "Ok";
        }
        throw new IllegalArgumentException("Subject already exist");

    }

    public Long getRoomOwnerId(Long roomId) {
        return repository.findById(roomId).get().getOwnerId();
    }

    public Page<AllRoomDto> findPaginatedRoom(Pageable pageable, String login, boolean isOwner, @Nullable String sort) {
        List<AllRoomDto> rooms;
        if (isOwner) {
            rooms = getRooms(login);
        } else {
            rooms = getStudentRoom(login);
        }

        switch (sort) {
            case "Name":
                rooms.sort(new Comparator<>() {
                    public int compare(AllRoomDto one, AllRoomDto two) {
                        return one.getName().compareTo(two.getName());}
                    });
            case "Description":
                rooms.sort(new Comparator<>() {
                    public int compare(AllRoomDto one, AllRoomDto two) {
                        return one.getDescription().compareTo(two.getDescription());}
                });
            default:
                break;
        }


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<AllRoomDto> list;

        if (rooms.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, rooms.size());
            list = rooms.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), rooms.size());
    }

    public Page<Task> findPaginatedTask(Pageable pageable, Long roomId, @Nullable String sort) {
        List<Task> tasks = repository.findById(roomId).get().getTasks();

        switch (sort) {
            case "Name":
                tasks.sort(new Comparator<>() {
                    public int compare(Task one, Task two) {
                        return one.getName().compareTo(two.getName());}
                });
            case "Description":
                tasks.sort(new Comparator<>() {
                    public int compare(Task one, Task two) {
                        return one.getDescribe().compareTo(two.getDescribe());}
                });
            case "Deadline":
                tasks.sort(new Comparator<>() {
                    public int compare(Task one, Task two) {
                        return one.getDeadline().compareTo(two.getDeadline());}
                });
            case "Comment":
                tasks.sort(new Comparator<>() {
                    public int compare(Task one, Task two) {
                        return one.getComment().compareTo(two.getComment());}
                });
            case "Resource":
                tasks.sort(new Comparator<>() {
                    public int compare(Task one, Task two) {
                        return one.getResource().getDescribe().compareTo(two.getResource().getDescribe());}
                });
            default:
                break;
        }



        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Task> list;

        if (tasks.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, tasks.size());
            list = tasks.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), tasks.size());
    }
}

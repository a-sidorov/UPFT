package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.upft.entity.user.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByType(String type);
}

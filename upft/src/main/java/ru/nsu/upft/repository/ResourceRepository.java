package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.upft.entity.room.Resource;

public interface ResourceRepository extends JpaRepository<Resource, Long> {
}

package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.upft.entity.user.UserHistory;

import java.sql.Timestamp;
import java.util.List;

public interface UserAuditRepository extends JpaRepository<UserHistory, Long> {

    /**
     * SELECT ua.id,ua.operation, ua.first_name, ua.second_name, ua.login, ua.password, to_char(ua.stamp, 'yyyy-mm-dd hh24:mi:ss') AS updated
     * FROM users u
     * LEFT OUTER JOIN users_audit ua ON ua.login = u.login
     * GROUP BY ua.id
     * HAVING max(ua.stamp) >= to_timestamp('2021-04-29', 'yyyy-mm-dd hh24:mi:ss');
     */

    @Query(value =
            """
                    SELECT ua.id as id,
                    ua.operation as operation,
                    ua.first_name as first_name,
                    ua.second_name as second_name,
                    ua.login as login,
                    ua.password as password,
                    to_char(ua.stamp, 'yyyy-mm-dd hh24:mi:ss') AS updated
                    FROM users u
                             LEFT OUTER JOIN users_audit ua ON ua.login = u.login
                    GROUP BY ua.id,ua.stamp
                    HAVING max(ua.stamp) >= :timestamp
                    order by ua.stamp desc""", nativeQuery = true)
    List<UserHistory> getAllUserHistory(Timestamp timestamp);
}

package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.upft.entity.room.Room;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends JpaRepository<Room, Long> {
    @Query(value =
            """
                    select * from rooms
                    where owner_id = (select users.id from users
                    where login = :login)
                    group by rooms.id, rooms.name
                    order by rooms.name
                        """,
            nativeQuery = true)
    List<Room> findRoomsByOwnerLogin(String login);

    @Query(value =
            """
                    select r.* from rooms as r
                    join users_rooms ur on r.id = ur.room_id
                    where ur.user_id = (select users.id from users
                                        where login = :login)
                    group by r.id
                    having r.id = :id
                        """,
            nativeQuery = true)
    Optional<Room> findByIdAndMemberLogin(Long id, String login);

    @Query(value =
            """
                    select * from rooms
                    where owner_id = (select users.id from users
                    where login = :login) and id = :id
                        """,
            nativeQuery = true)
    Optional<Room> findByIdAndOwnerLogin(Long id, String login);

    @Query(value =
            """
                    select count(*) from rooms
                    where owner_id = (select users.id from users
                        where login = :login)
                        """,
            nativeQuery = true)
    Long countRoomsByOwnerLogin(String login);

}

package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.upft.entity.user.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value =
            """
                    select users.* from users
                    inner join users_roles as ur on users.id = ur.user_id
                    inner join roles as r on r.id = ur.role_id
                    where r.type = :role""", nativeQuery = true)
    List<User> findAllByRole(@Param("role") String role);

    Optional<User> findByLogin(String login);
}

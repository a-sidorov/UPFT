package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.upft.entity.desk.Desk;
import ru.nsu.upft.entity.desk.DeskObject;

import java.util.List;

public interface DeskObjectRepository extends JpaRepository<DeskObject, Long> {
    List<DeskObject> findDeskObjectByDesk(Desk desk);
}

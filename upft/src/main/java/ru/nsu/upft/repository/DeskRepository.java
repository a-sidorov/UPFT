package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.upft.entity.desk.Desk;
import ru.nsu.upft.entity.user.Role;

public interface DeskRepository extends JpaRepository<Desk, Long> {
}

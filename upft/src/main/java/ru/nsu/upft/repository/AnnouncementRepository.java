package ru.nsu.upft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.upft.entity.room.Announcements;
import ru.nsu.upft.entity.room.Room;

import java.util.List;

public interface AnnouncementRepository extends JpaRepository<Announcements, Long> {
    Announcements findByRoom(Room room);
    List<Announcements> findAllByRoom(Room room);
}

package ru.nsu.upft.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.upft.dto.UserDtoRequest;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.exception.UserExistException;
import ru.nsu.upft.services.AuthService;

import javax.security.auth.message.AuthException;
import javax.validation.constraints.NotBlank;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<?> createUser(@RequestBody UserDtoRequest user) {
        if (user.getPassword().equals(user.getPassword_again())) {
            try {
                return ResponseEntity.ok(authService.createUser(user));
            } catch (DataNotFoundException | UserExistException e) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Passwords is not equal");
        }
    }


    @PostMapping("/login")
    public ResponseEntity<?> auth(@NotBlank @RequestHeader("Authorization") String authorizationHeader) {
        try {
            return ResponseEntity.ok(authService.createAuthToken(authorizationHeader));
        } catch (AuthException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Wrong password or login");
        }
    }
}

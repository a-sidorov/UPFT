package ru.nsu.upft.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.nsu.upft.dto.UserDtoRequest;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.services.UserService;

@RestController
@RequiredArgsConstructor
public class UserController {
    private static final String USERS_PATH = "/users";
    private final UserService userService;


    //    @PreAuthorize("#user.login == authentication.principal.username")
    @PreAuthorize("hasAnyRole('ADMIN','TEACHER','STUDENT')")
    @PostMapping(USERS_PATH + "/update")
    public ResponseEntity<?> updateUser(@RequestBody UserDtoRequest user) {
        try {
            var userName = SecurityContextHolder.getContext().getAuthentication().getName();
            return ResponseEntity.ok(userService.updateUser(user, userName));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @GetMapping(USERS_PATH + "/me")
    public ResponseEntity<?> getUserInfo() throws DataNotFoundException {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        var user = userService.getUserByLogin(userName);
        var dto = userService.getUserMapper().userToDtoResponse(user);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasAnyRole('TEACHER','ADMIN','STUDENT')")
    @DeleteMapping(USERS_PATH + "/remove")
    public ResponseEntity<?> removeUser() {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            userService.remove(userName);
            return ResponseEntity.ok("Done");
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(USERS_PATH + "/all")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(USERS_PATH + "/{userId}")
    public ResponseEntity<?> getUser(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(userService.getUser(userId));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(USERS_PATH + "/role/{role}")
    public ResponseEntity<?> getUserByRole(@PathVariable String role) {

        if (role.equalsIgnoreCase("ROLE_STUDENT") || role.equalsIgnoreCase("ROLE_TEACHER")) {
            return ResponseEntity.ok(userService.getUsersByRole(role));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(USERS_PATH + "/audit")
    public ResponseEntity<?> getAudit(@RequestBody String data) {
        return ResponseEntity.ok(userService.getHistory(data));
    }
}

package ru.nsu.upft.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.nsu.upft.dto.AnnouncementRequestDto;
import ru.nsu.upft.dto.RoomDto;
import ru.nsu.upft.dto.SubjectDto;
import ru.nsu.upft.dto.TaskDto;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.services.RoomService;


@RestController
@RequiredArgsConstructor
public class RoomController {
    private final static String ROOM_PATH = "/rooms";
    private final RoomService roomService;

    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN')")
    @PostMapping(ROOM_PATH + "/create")
    ResponseEntity<?> createRoom(@RequestBody  RoomDto room) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(roomService.createRoom(userName, room));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN')")
    @PostMapping(ROOM_PATH + "/remove")
    ResponseEntity<?> removeRoom(@RequestBody @Nullable Long roomId) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            roomService.removeRoom(userName, roomId);
            return ResponseEntity.ok("Removed");
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN','STUDENT')")
    @GetMapping(ROOM_PATH)
    ResponseEntity<?> getRooms() {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(roomService.getRooms(userName));
    }

    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN')")
    @PostMapping(ROOM_PATH + "/{roomId}/invite")
    ResponseEntity<?> inviteUsers(@PathVariable Long roomId, @RequestBody String userLogin) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();

        try {
            roomService.inviteStudents(userName, roomId, userLogin);
            return ResponseEntity.ok("User invited");
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN')")
    @PostMapping(ROOM_PATH + "/{roomId}/add/task")
    ResponseEntity<?> createTask(@PathVariable Long roomId, @RequestBody TaskDto taskDto) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(roomService.createTask(userName, taskDto, roomId));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN')")
    @PostMapping(ROOM_PATH + "/{roomId}/add/announcement")
    ResponseEntity<?> addAnnouncement(@PathVariable Long roomId, @RequestBody AnnouncementRequestDto dto) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(roomService.addAnnouncement(userName, roomId, dto));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('TEACHER', 'ADMIN','STUDENT')")
    @GetMapping(ROOM_PATH + "/{roomId}/get/announcement" )
    ResponseEntity<?> getRoomAnnouncements(@PathVariable Long roomId) {

        try {
            return ResponseEntity.ok(roomService.getAllRoomAnnouncements(roomId));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping(ROOM_PATH + "/subjects")
    ResponseEntity<?> getSubjects(){
        var list = roomService.getAllSubjects();
        return ResponseEntity.ok(list);
    }

    @PostMapping(ROOM_PATH + "/add/subject")
    ResponseEntity<?> addSubjects(@RequestBody SubjectDto dto){
        try {
        return ResponseEntity.ok(roomService.createSubject(dto));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Subject exist");
        }
    }

    @GetMapping(ROOM_PATH + "/{roomId}/get/owner/id")
    ResponseEntity<?> getOwnerId(@PathVariable Long roomId){
        return ResponseEntity.ok(roomService.getRoomOwnerId(roomId));
    }

}

package ru.nsu.upft.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.services.DeskService;

@RestController
@RequiredArgsConstructor
public class DeskController {
    private final DeskService deskService;

    @PreAuthorize("hasAnyRole('ADMIN','TEACHER','STUDENT')")
    @GetMapping("/desk/all/{id}")
    ResponseEntity<?> getAllObjects(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(deskService.getAllObjects(id));
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}

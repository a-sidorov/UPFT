package ru.nsu.upft.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.nsu.upft.services.DeskService;

@Controller
@RequiredArgsConstructor
public class SocketController {
    private final DeskService deskService;

    private final SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/save/{id}")
    public void saveDesk(@DestinationVariable String id, @RequestBody String json) {
        var res = deskService.addObject(Long.valueOf(id), json);

        messagingTemplate.convertAndSendToUser(
                id,
                "/queue/objects",
                res
        );
    }

    @RequestMapping(value = "/hi")
    public String getDesk() {
        return "desk";
    }
}

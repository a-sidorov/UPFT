package ru.nsu.upft.controllers;

import com.sun.istack.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.nsu.upft.dto.AllRoomDto;
import ru.nsu.upft.entity.room.Room;
import ru.nsu.upft.entity.room.Task;
import ru.nsu.upft.exception.DataNotFoundException;
import ru.nsu.upft.services.RoomService;
import ru.nsu.upft.services.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequiredArgsConstructor
public class MainController {

    private final UserService userService;
    private final RoomService roomService;

    @RequestMapping(value = "/")
    public String getLoginPage() {
        return "login";
    }


    @RequestMapping(value = "/main")
    public ModelAndView getMainPage(
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("pageAlt") Optional<Integer> pageAlt,
            @RequestParam("sortOwn")  Optional<String> fieldOwn,
            @RequestParam("sortStudent") Optional<String> fieldStd
    ) throws DataNotFoundException {
        var map = new ModelAndView("main");
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();


        var user = userService.getUserByLogin(userName);
        map.addObject("usr", user);
        int currentPage = page.orElse(1);
        int pageSize = 2;

        Page<AllRoomDto> roomPage = roomService.findPaginatedRoom(PageRequest.of(currentPage - 1, pageSize), userName, true, fieldOwn.orElse("none"));

        int totalPages = roomPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            map.addObject("pageNumbersOwn", pageNumbers);
        }

        map.addObject("yourRoomOwn", roomPage);
        int altPage = pageAlt.orElse(1);
        Page<AllRoomDto> roomStudentPage = roomService.findPaginatedRoom(PageRequest.of(altPage - 1, pageSize), userName, false, fieldStd.orElse("none"));

        int totalPagesStudent = roomStudentPage.getTotalPages();
        if (totalPagesStudent > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPagesStudent)
                    .boxed()
                    .collect(Collectors.toList());
            map.addObject("pageNumbersStudent", pageNumbers);
        }

        map.addObject("yourRoomStudent", roomStudentPage);
        return map;
    }

    @RequestMapping(value = "/room/{id}")
    public ModelAndView getRoomPage(@PathVariable Long id, @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("sort") Optional<String> field) throws DataNotFoundException {
        var map = new ModelAndView("room");

        var userName = SecurityContextHolder.getContext().getAuthentication().getName();

        Room room = null;
        try {
            room = roomService.getRoom(userName, id);
        } catch (DataNotFoundException e) {
            map.setStatus(HttpStatus.NOT_FOUND);
            return map;
        }
        map.addObject("room", room);
        map.addObject("usr", room.getOwner());
        map.addObject("std", room.getUsers());
        map.addObject("id", id);
        var anon = roomService.getAllRoomAnnouncements(id);
        map.addObject("announcements", anon);


        int currentPage = page.orElse(1);
        int pageSize = 2;

        Page<Task> tasks = roomService.findPaginatedTask(PageRequest.of(currentPage - 1, pageSize), id, field.orElse("none"));

        int totalPages = tasks.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            map.addObject("pageNumbersOwn", pageNumbers);
        }

        map.addObject("tasks", tasks);
        return map;
    }

    @RequestMapping(value = "/room/desk/{id}")
    public String getDeskPage(@PathVariable Long id) {
        return "desk";
    }


}

package ru.nsu.upft.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter
@Getter
@NoArgsConstructor
public class RoomDto {
    private String name;
    private String description;
    private SubjectDto subject;
}

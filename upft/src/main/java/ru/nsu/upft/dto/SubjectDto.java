package ru.nsu.upft.dto;

import com.sun.istack.Nullable;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter
@Getter
@NoArgsConstructor
public class SubjectDto {
    private String type;
    @Nullable
    private String describe;

    public SubjectDto(String type, String describe) {
        this.type=type;
        this.describe = describe;
    }
}

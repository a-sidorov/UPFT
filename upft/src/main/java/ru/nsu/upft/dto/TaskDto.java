package ru.nsu.upft.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Data
@Setter
@Getter
@NoArgsConstructor
public class TaskDto {
    private String name;
    private Date deadline;
    private String description;
    private String comment;
    private ResourceDto resource;
}

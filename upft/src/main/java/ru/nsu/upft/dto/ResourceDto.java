package ru.nsu.upft.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter
@Getter
@NoArgsConstructor
public class ResourceDto {
    private String linkToResourse;
    private String describe;
}

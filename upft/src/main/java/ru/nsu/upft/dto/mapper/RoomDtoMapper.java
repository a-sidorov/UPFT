package ru.nsu.upft.dto.mapper;

import org.springframework.stereotype.Component;
import ru.nsu.upft.dto.AllRoomDto;
import ru.nsu.upft.entity.room.Room;

@Component
public class RoomDtoMapper {
    public AllRoomDto toDto(Room room) {
        var roomDto = new AllRoomDto();
        roomDto.setId(room.getId());
        roomDto.setName(room.getName());
        roomDto.setDescription(room.getDescribe());
        return roomDto;
    }
}

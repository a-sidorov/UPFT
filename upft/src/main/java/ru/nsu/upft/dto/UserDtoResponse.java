package ru.nsu.upft.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Data
@Setter
@Getter
@NoArgsConstructor
public class UserDtoResponse {
    private Long id;

    private  String firstName;

    private  String secondName;

    private  String login;

    private Set<String> roles;
}

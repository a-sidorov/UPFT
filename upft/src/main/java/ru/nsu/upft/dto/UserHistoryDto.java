package ru.nsu.upft.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.nsu.upft.entity.user.UserHistory;

@RequiredArgsConstructor
@Getter
public class UserHistoryDto {
    private final String operation;
    private final String firstName;
    private final String secondName;
    private final String login;
    private final String updated;

    public static UserHistoryDto toDto(UserHistory userHistory) {
        return new UserHistoryDto(userHistory.getOperation(),
                userHistory.getFirstName(),
                userHistory.getSecondName(),
                userHistory.getLogin(),
                userHistory.getUpdated());
    }
}

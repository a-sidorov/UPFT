package ru.nsu.upft.dto;

import lombok.*;

import java.sql.Timestamp;

@Data
@Setter
@Getter
@AllArgsConstructor
public class AnnouncementResponceDto {
    private String text;
    private Timestamp creationDate;
}

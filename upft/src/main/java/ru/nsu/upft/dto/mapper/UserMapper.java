package ru.nsu.upft.dto.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.lang.NonNull;
import ru.nsu.upft.dto.UserDtoRequest;
import ru.nsu.upft.dto.UserDtoResponse;
import ru.nsu.upft.entity.user.Role;
import ru.nsu.upft.entity.user.User;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserMapper {


    UserDtoResponse userToDtoResponse(User user);


    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "roles", ignore = true),
            @Mapping(target = "roomsByOwn", ignore = true),
            @Mapping(target = "registrationDate", ignore = true)
    })
    User dtoToUser(UserDtoRequest userDto);

    default String map(@NonNull Role role) {
        return role.getType();
    }
}

package ru.nsu.upft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpftApplication {
    public static void main(String[] args) {
        SpringApplication.run(UpftApplication.class, args);
    }

}

package ru.nsu.upft.entity.user;


import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.NonNull;
import ru.nsu.upft.entity.room.Room;
import ru.nsu.upft.entity.idenifiable.Identifiable;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name = "users")
public class User extends Identifiable {
    @NonNull
    private String firstName;

    @NonNull
    private String secondName;

    @Column(unique = true)
    private String login;

    @NonNull
    private String password;

    private Timestamp registrationDate = Timestamp.valueOf(LocalDateTime.now());

    //todo: Основной Предмет ManyToOne

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
    )
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Set<Role> roles = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Room> roomsByOwn = new ArrayList<>();


}

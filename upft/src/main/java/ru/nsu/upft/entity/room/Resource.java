package ru.nsu.upft.entity.room;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.upft.entity.idenifiable.Identifiable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "resource")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Resource extends Identifiable {

    private String linkToResource;
    private String describe;
}

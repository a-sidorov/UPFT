package ru.nsu.upft.entity.user;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
public class UserHistory {
    @Id
    @Column(columnDefinition = "id")
    private Long id;
    @Column(columnDefinition = "operation")
    private String operation;
    @Column(columnDefinition = "first_name")
    private String firstName;
    @Column(columnDefinition = "second_name")
    private String secondName;
    @Column(columnDefinition = "login")
    private String login;
    @Column(columnDefinition = "password")
    private String password;
    @Column(columnDefinition = "updated")
    private String updated;
}

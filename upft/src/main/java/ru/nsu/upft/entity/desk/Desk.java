package ru.nsu.upft.entity.desk;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.nsu.upft.entity.idenifiable.Identifiable;
import ru.nsu.upft.entity.room.Room;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "desk")
public class Desk extends Identifiable {
    @OneToOne
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Room room;

    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "desk_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<DeskObject> objets = new ArrayList<>();

    public Desk(String aDefault) {
        this.name = aDefault;
    }
}

package ru.nsu.upft.entity.room;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.nsu.upft.entity.idenifiable.Identifiable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "announcements")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Announcements extends Identifiable {
    private String text;
    private Timestamp creationDate =  Timestamp.valueOf(LocalDateTime.now());

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Room room;
}

package ru.nsu.upft.entity.desk;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.nsu.upft.entity.idenifiable.Identifiable;

import javax.persistence.*;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "desk_objects")
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class
)
public class DeskObject extends Identifiable {

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private String data;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desk_id", referencedColumnName = "id")
    private Desk desk;
}

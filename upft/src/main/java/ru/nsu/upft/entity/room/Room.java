package ru.nsu.upft.entity.room;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.nsu.upft.entity.desk.Desk;
import ru.nsu.upft.entity.idenifiable.Identifiable;
import ru.nsu.upft.entity.user.User;

import javax.persistence.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Entity
@Table(name = "rooms")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Room extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private User owner;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(
            name = "users_rooms",
            joinColumns = @JoinColumn(name = "room_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private List<User> users;

    private String name;
    private String describe;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id", referencedColumnName = "room_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Desk roomDesk;

    @OneToMany
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    private List<Task> tasks;
//    @OneToMany
//    private List<Announcements> announcementsList;

    @ManyToOne
    @JoinColumn(name = "subject_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Subject subject;


    public boolean hasMember(String userLogin){
        AtomicBoolean isMember = new AtomicBoolean(false);
        users.forEach(a->{
            if(a.getLogin().equals(userLogin))
                isMember.set(true);
        });

        return isMember.get();
    }

    public Long getOwnerId(){
        return owner.getId();
    }
}

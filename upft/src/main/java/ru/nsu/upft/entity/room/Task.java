package ru.nsu.upft.entity.room;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.upft.entity.idenifiable.Identifiable;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tasks")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Task extends Identifiable {

    private String name;
    private Date deadline;

    @ManyToOne
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    private Room room;

    private String describe;

    @OneToOne
    @JoinColumn(name = "resource_id", referencedColumnName = "id")
    private Resource resource;

    private String comment;
}

package ru.nsu.upft.entity.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import ru.nsu.upft.entity.idenifiable.Identifiable;

import javax.persistence.Entity;
import javax.persistence.Table;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "roles")
@Entity
public class Role extends Identifiable implements GrantedAuthority {
    private String type;

    @Override
    public String getAuthority() {
        return type;
    }
}

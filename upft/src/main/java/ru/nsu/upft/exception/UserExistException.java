package ru.nsu.upft.exception;

public class UserExistException extends Exception {
    public UserExistException(String message) {
        super(message);
    }
}
